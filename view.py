from    PyQt5.uic           import  loadUi
from    PyQt5.QtWidgets     import  QMainWindow, QTableWidgetItem, QHeaderView
from    PyQt5.QtCore        import  Qt

from global_variables   import  *

class View(QMainWindow):
    def __init__(self):
        super(View, self).__init__()
        loadUi('UI/layout.ui', self)

        self.setFixedSize(self.size())
        self.tw_order_list.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)

        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_DISH, 215)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_AMOUNT, 100)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_TIME, 130)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_STATUS, 150)

        self.COLOR_DEFAULT_BACKGROUND      =   self.lw_table_list.item(0).background()
        self.COLOR_DEFAULT_FOREGROUND      =   self.lw_table_list.item(0).foreground()

        self.lw_table_list.setCurrentRow(0)

    def change_table_color(self, table_id, color_to_change):

        self.lw_table_list.item(table_id).setBackground(color_to_change)

        if (color_to_change     ==  COLOR_QUEUING_ORDER):
            self.lw_table_list.item(table_id).setForeground(COLOR_BLACK)
            return

        if (color_to_change     ==  COLOR_NEW_ORDER):
            self.lw_table_list.item(table_id).setForeground(COLOR_WHITE)
            return

        if (color_to_change     ==  COLOR_DONE):
            self.lw_table_list.item(table_id).setForeground(COLOR_WHITE)
            return

        if (color_to_change     ==  self.COLOR_DEFAULT_BACKGROUND):
            self.lw_table_list.item(table_id).setForeground(self.COLOR_DEFAULT_FOREGROUND)
            return

    def show_selected_table(self, table_order_list, menu):
        self.tw_order_list.setRowCount(0)

        for order in (table_order_list):
            row_current     =   self.tw_order_list.rowCount()

            item_id         =   order['item_id']
            item_name       =   menu[item_id]['name']

            wi_dish         =   QTableWidgetItem(item_name)
            wi_amount       =   QTableWidgetItem(str(order['amount']))
            wi_time         =   QTableWidgetItem(str(order['time']))

            if (order['status'] == IS_QUEUING):
                wi_status   =   QTableWidgetItem("Đang chờ")
            elif (order['status'] == IS_DELIVERING):
                wi_status   =   QTableWidgetItem("Đang giao")
            else:
                wi_status   =   QTableWidgetItem("Xong")

            wi_amount.setTextAlignment(Qt.AlignCenter)
            wi_time.setTextAlignment(Qt.AlignCenter)
            wi_status.setTextAlignment(Qt.AlignCenter)

            wi_check_box    =   QTableWidgetItem()

            if (order['is_ready']):
                wi_check_box.setCheckState(Qt.Checked)
            else:
                wi_check_box.setCheckState(Qt.Unchecked)

            wi_check_box.setFlags(wi_check_box.flags() ^ Qt.ItemIsUserCheckable)

            # if not queuing, disable checkbox
            if (order['status']      !=  IS_QUEUING):
                wi_check_box.setFlags(wi_check_box.flags() ^ Qt.ItemIsEnabled)

            self.tw_order_list.insertRow(row_current)

            self.tw_order_list.setItem(row_current, COLUMN_DISH, wi_dish)
            self.tw_order_list.setItem(row_current, COLUMN_AMOUNT, wi_amount)
            self.tw_order_list.setItem(row_current, COLUMN_TIME, wi_time)
            self.tw_order_list.setItem(row_current, COLUMN_STATUS, wi_status)
            self.tw_order_list.setItem(row_current, COLUMN_READY, wi_check_box)

    def toggle_checkbox(self, clicked_row):
        this_checkbox   =   self.tw_order_list.item(clicked_row, COLUMN_READY)

        if (this_checkbox.checkState() == Qt.Checked):
            this_checkbox.setCheckState(Qt.Unchecked)
        else:
            this_checkbox.setCheckState(Qt.Checked)