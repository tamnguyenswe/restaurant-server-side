*This is one part of the smart restaurant project, which controls the activities on server side*

The client side controling part:
https://gitlab.com/presariohg/restaurant-menu-python-3

**TODO**
+ Unselect item after clicked twice
+ Find a efficient way to retrieve wasted ids
+ Dev waiter's functions

+ traverse through menu

# Documentation / Manual
You can find the manual [here](https://drive.google.com/file/d/1jU0bD2fhAyh4zzXR8TzrtsNqmVUe9p72/view?usp=sharing) (Vietnamese)

# Installation Guide

#### Update OS
```bash
sudo apt-get update 
sudo apt-get upgrade
```
#### Install Python 3
```bash
sudo apt-get install python3
```
#### Install SIP:
```bash
sudo apt-get install python-sip-dev
```
#### Install PyQt5 library
```bash
sudo apt-get install python3-pyqt5
```
#### Install MQTT libraries 
```bash
sudo apt-get install mosquitto
sudo apt-get install mosquitto-clients
sudo pip3 install paho-mqtt
```
#### Install sox (which controls the beep sounds)
```bash
sudo apt-get install sox
```
## Run:
```bash
python3 server.py
```
## Troubleshoot:
By default mosquitto initiates a local MQTT server at start, but if you ran into some problems related to mosquitto server like:
> ConnectionRefusedError: [Errno 111] Connection refused

you should try restarting mosquitto:
```bash
pkill mosquitto
mosquitto
```