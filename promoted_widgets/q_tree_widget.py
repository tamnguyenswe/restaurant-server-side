from    PyQt5.QtWidgets     import  QTreeWidget
from    PyQt5.QtCore        import  QItemSelectionModel

class Q_Tree_Widget(QTreeWidget):

    def mousePressEvent(self, event):
        super(QTreeWidget, self).mousePressEvent(event)
        
        self.setCurrentItem(self.currentItem(), self.currentColumn(), QItemSelectionModel.Clear)

    def mouseMoveEvent(self, event):
        # disable mouse drag
        pass