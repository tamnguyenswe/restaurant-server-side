from    PyQt5.QtWidgets     import  QTableWidget


class Q_Table_Widget(QTableWidget):

    def mousePressEvent(self, event):
        # find which row is just clicked
        clicked_item    =   self.itemAt(event.pos())
        clicked_row     =   self.row(clicked_item)

        super(QTableWidget, self).mousePressEvent(event)

        # select that row
        self.selectRow(clicked_row)

    def mouseDoubleClickEvent(self, event):
        self.selectRow(self.currentRow())
