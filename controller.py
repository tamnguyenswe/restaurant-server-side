from    PyQt5.QtCore            import  QThreadPool, Qt

from    view                    import  View
from    model                   import  Model, Thread_MQTT, Thread_Beep

from    global_variables        import  *
from    class_definitions       import  Table

class Controller:
    def __init__(self):
        self.view                       =   View()
        self.model                      =   Model()

        self.thread_pool                =   QThreadPool()

        self.mqtt_init()
        self.mqtt_thread                =   Thread_MQTT(self)
        self.thread_pool.start(self.mqtt_thread)

        self.view.bt_deliver.clicked.connect(self.deliver)
        self.view.bt_checkout.clicked.connect(self.check_out)
        self.view.lw_table_list.itemClicked.connect(self.show_selected_table)
        self.view.tw_order_list.itemClicked.connect(self.toggle_checkbox)

        self.view.ac_update_menu.triggered.connect(self.update_menu)
        self.view.show()

    def deliver(self):
        self.model.deliver()
        self.show_selected_table()

    def update_menu(self, checked):
        from    menu_update import  menu_update_controller as menu_update

        menu_update.Controller()

        # update new menu
        self.model.load_menu()

    def check_out(self):
        from    check_out   import  check_out_controller as check_out

        table_id    =   self.view.lw_table_list.currentRow()
        order_list  =   self.model.table[table_id].order

        check_out_dialog    =   check_out.Controller(order_list, self.model.menu_items, table_id)
        if (check_out_dialog.is_check_out_accepted):
            # clear table after checking out
            self.model.table[table_id]    =   Table()
            self.show_selected_table()

    ##########################################

    def on_connect(self, client, userdata, flags, rc):
        self.model.client.subscribe(TOPIC_NEW_ORDER)
        self.model.client.subscribe(TOPIC_WAITER)

    def on_message(self, client, userdata, message):
        message_content     =   message.payload.decode('utf-8')

        if (message.topic   ==  TOPIC_NEW_ORDER):
            self.beep()
            self.model.receive_order(message_content)

        elif (message.topic ==  TOPIC_WAITER):
            self.model.receive_waiter_report(message_content)

        self.view.change_table_color(self.model.table_to_change_color, self.model.color_to_change)

        self.show_selected_table()

    def mqtt_init(self):
        self.model.client.on_connect    =   self.on_connect
        self.model.client.on_message    =   self.on_message

        try:
            self.model.client.connect("127.0.0.1", 1883)
        except ConnectionRefusedError:
            from PyQt5.QtWidgets    import QMessageBox
            import sys

            QMessageBox.critical(self.view, 'Lỗi', 'Server MQTT không online')
            
            sys.exit()

    ##########################################

    def show_selected_table(self):
        table_id        =   self.view.lw_table_list.currentRow()

        color_current   =   self.view.lw_table_list.item(table_id).background()

        if (color_current   ==  COLOR_NEW_ORDER):
            self.view.change_table_color(table_id, COLOR_QUEUING_ORDER)
        else:
            self.view.change_table_color(table_id, self.view.COLOR_DEFAULT_BACKGROUND)

        # self.view.show_selected_table(self.model.order_of_table[table_id])
        self.view.show_selected_table(self.model.table[table_id].order, self.model.menu_items)

    def toggle_checkbox(self, clicked_item):
        clicked_row     =   self.view.tw_order_list.row(clicked_item)
        self.view.tw_order_list.selectRow(clicked_row)

        table_id        =   self.view.lw_table_list.currentRow()
        order_id        =   clicked_row

        clicked_order   =   self.model.table[table_id].order[order_id]

        # only when queuing can toggle checkbox
        if (clicked_order['status']   ==    IS_QUEUING):
            self.view.toggle_checkbox(clicked_row)

            clicked_checkbox            =   self.view.tw_order_list.item(clicked_row, COLUMN_READY)
            clicked_order['is_ready']   =   (clicked_checkbox.checkState() == Qt.Checked)

    def beep(self):
        self.thread_beep    =   Thread_Beep()
        self.thread_pool.start(self.thread_beep)
