from PyQt5.QtWidgets    import  QDialog, QTableWidgetItem
from PyQt5.QtCore       import  Qt
from PyQt5.uic          import  loadUi

COLUMN_DISH         =   0
COLUMN_AMOUNT       =   1
COLUMN_PRICE        =   2
COLUMN_TOTAL        =   3

class View(QDialog):
    def __init__(self, table_id):
        super(View, self).__init__()
        loadUi('./UI/check_out.ui', self)

        self.setFixedSize(self.size())

        self.setWindowTitle('Bàn {} thanh toán'.format(table_id + 1))

        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_DISH, 250)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_AMOUNT, 100)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_PRICE, 130)
        self.tw_order_list.horizontalHeader().resizeSection(COLUMN_TOTAL, 200)

        self.tw_order_list.setRowCount(0)

    def add_item(self, item_name, amount, price):
        last_row        =   self.tw_order_list.rowCount()

        total           =   int(amount) * int(price)

        wi_item_name    =   QTableWidgetItem(item_name)
        wi_amount       =   QTableWidgetItem(str(amount))
        wi_price        =   QTableWidgetItem('{} VND'.format(price))
        wi_total        =   QTableWidgetItem('{} VND'.format(total))

        wi_amount.setTextAlignment(Qt.AlignCenter)
        wi_price.setTextAlignment(Qt.AlignCenter)
        wi_total.setTextAlignment(Qt.AlignCenter)

        self.tw_order_list.insertRow(last_row)

        self.tw_order_list.setItem(last_row, COLUMN_DISH, wi_item_name)
        self.tw_order_list.setItem(last_row, COLUMN_AMOUNT, wi_amount)
        self.tw_order_list.setItem(last_row, COLUMN_PRICE, wi_price)
        self.tw_order_list.setItem(last_row, COLUMN_TOTAL, wi_total)

    def show_total(self, order_total):
        self.lb_sum.setText("Thành tiền: {} VND".format(order_total))
